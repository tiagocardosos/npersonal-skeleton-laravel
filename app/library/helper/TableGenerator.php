<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Omicron
 * Date: 24/07/13
 * Time: 16:18
 * To change this template use File | Settings | File Templates.
 */

namespace Helper;

use Helper\WarningBox;


class TableGenerator
{
    public $headers;
    public $content;
    public $value;

    private function headerGenerate()
    {
        if (!empty($this->headers) && is_array($this->headers)) {
            $headersContent = null;
            foreach ($this->headers as $value) {
                $headersContent .= "<th>" . $value . "</th>";
            }
            return "<thead><tr>" . $headersContent . "</tr></thead>";
        } else {
            return FALSE;
        }
    }

    private function valueGenerate($valueContent, $keyContent)
    {


        if (!empty($this->value) && is_array($this->value)) {

            foreach ($this->value as $key => $value) {
                if ($key == $keyContent) {
                    $returnValue = $value['type'];
                }

                return $returnValue;
            }
        } else {
            return $valueContent;
        }
    }

    private function contentGenerate()
    {

        if (is_array($this->headers) && is_array($this->content)) {

            for ($arrayCount = 0; $arrayCount < count($this->content); $arrayCount++) {

                $contentKey = array_keys($this->content[$arrayCount]);

                $headersKey = array_keys($this->headers);

                $dataShowArray = array_intersect($headersKey, $contentKey);

                foreach ($dataShowArray as $value) {

                    $dataContent[$arrayCount][$value] = $this->content[$arrayCount][$value];

                }
            }

            $this->content = $dataContent;

        }


        $contentReturn = null;

        if (empty($this->content) || !is_array($this->content)) {
            return false;
        }

        foreach ($this->content as $valueContent) {
            $contentReturn .= "<tr>";
            foreach ($valueContent as $key => $value) {
                $contentReturn .= "<td>";
                $contentReturn .= $this->valueGenerate($value, $key);
                $contentReturn .= "</td>";
            }
            $contentReturn .= "</tr>";
        }
        return "<div class=\"table-responsive\"><table class=\"table table-bordered table-hover table-striped\">" . $this->headerGenerate() . $contentReturn . "</table></div>";

    }

    public function tableGenerate()
    {

        if (empty($this->content) || !is_array($this->content)) {
            $warningBox = new WarningBox;
            $warningBox->typeBox = 2;
            $warningBox->contentBox = "<h4>Aviso!</h4> Nenhum conteúdo encontrado";
            return $warningBox->boxGenerate();
        }

        return $this->contentGenerate();
    }
}