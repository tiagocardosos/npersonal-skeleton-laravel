<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Helper;
/**
 * Description of Sendrequests
 *
 * @author Omicron
 */
class SendRequest {

    public $customRequest = null;
    public $headerInformation = null;
    public $postInformation = array('');
    public $decrypt = FALSE;
    public $encrypt = FALSE;
    private $_secretArray;
    private $_securityCrypt;

    public function __construct($secretArray = array()) {

        
        $this->_secretArray = $secretArray;
        $this->_securityCrypt = new \Helper\Securitycrypt;
        

    }

    private function decryptData($encryptData) {


        if ($this->decrypt == TRUE) {
            
            $key = $this->_securityCrypt->pbkdf2($this->_secretArray['pass'], $this->_secretArray['salt'], 1000, 32);
                

            $returnData = $this->_securityCrypt->decrypt($encryptData, $key);
        } else {
            $returnData = $encryptData;
        }

        return $returnData;
    }

    private function cryptData($data) {

        if ($this->encrypt == TRUE) {

            $key = $this->_securityCrypt->pbkdf2($this->_secretArray['pass'], $this->_secretArray['salt'], 1000, 32);


            $returnData = $this->_securityCrypt->encrypt($data, $key);
        } else {
            $returnData = $data;
        }

        return $returnData;
    }

    /**
     * set - 
     * @param array $prop Array de dados
     * @param mixed $value Valores mistos
     * @return \Helper\SendRequests
     */
    public function set($prop, $value = null) {
        if (is_array($prop)) {
            foreach ($prop as $key => $propValue) {
                $this->$key = $propValue;
            }
        } else {
            $this->$prop = $value;
        }

        return $this;
    }

    /**
     * requests - 
     * @param string $url String
     * @return mixed
     */
    public function requests($url = null) {

        $string_informations = null;

        // surfing in array      
        if (is_array($this->postInformation)) {
            foreach ($this->postInformation as $name => $value) {
                $string_informations .= $name . '=' . $value . '&';
            }
        } else {
            $string_informations .= $this->postInformation;
        }

        rtrim($string_informations, '&');

        $string_informations = $this->cryptData($string_informations);

        $ch = \curl_init();

        // config curl options
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->customRequest);
        curl_setopt($ch, CURLOPT_VERBOSE, false);

        if (!empty($this->headerInformation)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headerInformation);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // recall requisition return
        // curl_setopt($ch, CURLOPT_HEADER, 1);
        if (!empty($this->postInformation)) {
            //curl_setopt($ch, CURLOPT_POST, count($this->postInformation));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $string_informations);
        }

        // send post requisition
        $exeCurl = curl_exec($ch);
        $header_size = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        
        // send verification
        if ($this->customRequest == 'HEAD') {
            $return["return"] = true;
            $return["headerInformation"] = $header_size;
        } elseif (!$exeCurl) {
            $return["return"] = false;
            $return["errorMessage"] = "The server don't receive a Response / SendRequests";
        } else {
            
            $return["return"] = $this->decryptData($exeCurl);
            $return["headerInformation"] = $header_size;
        }

        curl_close($ch); // close the curl protocol
        return $return;
    }

    public function get($url, $debug = false) {

        $this->customRequest = 'GET';

        $dataRequest = $this->requests($url);
        if($debug == true){
            var_dump($dataRequest['return']);exit;
        }
        $returnData = $dataRequest['return'];

        
        if(!empty($returnData)){
            return $returnData;
        }else{
            return FALSE;
        }
        
    }
 
    public function post($url, $arrayInformation, $debug = false) {

        $this->customRequest = 'POST';
        $this->postInformation = $arrayInformation;

        $dataRequest = $this->requests($url);
        
        if($debug == true){
            var_dump($dataRequest['return']);exit;
        }
        
        $returnData = json_decode($dataRequest['return'], true);

        
        if(!empty($returnData)){
            return $returnData;
        }else{
            return FALSE;
        }
    }

    public function put($url, $arrayInformation, $debug = false) {

        $this->customRequest = 'PUT';
        $this->postInformation = $arrayInformation;

        $dataRequest = $this->requests($url);
        if($debug == true){
            var_dump($dataRequest['return']);exit;
        }
        $returnData = json_decode($dataRequest['return'], true);

        
        if(!empty($returnData)){
            return $returnData;
        }else{
            return FALSE;
        }
    }

    public function delete($url, $debug = false) {

        $this->customRequest = 'DELETE';

        $dataRequest = $this->requests($url);
        if($debug == true){
            var_dump($dataRequest['return']);exit;
        }
        $returnData = json_decode($dataRequest['return'], true);

        
        if(!empty($returnData)){
            return $returnData;
        }else{
            return FALSE;
        }
    }

}

?>
