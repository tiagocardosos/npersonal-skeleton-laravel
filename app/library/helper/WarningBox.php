<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Helper;

/**
 *
 * @author Omicron
 */
class WarningBox {

    public $contentBox = null;
    public $typeBox = null;

    public function boxGenerate() {
        $boxCode = null;

        if ($this->typeBox == 1) {
            $classType = 'alert-warning';
        }
        if ($this->typeBox == 2) {
            $classType = 'alert-danger';
        }
        if ($this->typeBox == 3) {
            $classType = 'alert-success';
        }

        if ($this->typeBox == 4) {
            $classType = 'alert-info';
        }


        $boxCode .= "<div class=' alert " . $classType . "'>
                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
                    " . $this->contentBox . "
                    </div>";
        return $boxCode;
    }

}

?>
