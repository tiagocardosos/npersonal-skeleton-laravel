<?php

namespace Projeto;

use \Helper\WarningBox;
use \Helper\TableGenerator;

class BaseController extends \Controller {

    public function __construct()
    {


        \Asset::style('bootstrap', 'plugins/bootstrap/css/bootstrap.css');
        \Asset::style('datetimepicker', 'plugins/datetimepicker/datetimepicker.min.css');
        \Asset::style('chosen', 'plugins/chosen/chosen.css');
        \Asset::style('toastmessage', 'plugins/toastmessage/css/jquery.toastmessage.css');

        \Asset::add('jquery', 'js/jquery.js');
        \Asset::add('boostrap', 'plugins/bootstrap/js/bootstrap.js');
        \Asset::add('datetimepicker', 'plugins/datetimepicker/datetimepicker.min.js');
        \Asset::add('chosen', 'plugins/chosen/chosen.jquery.js');
        \Asset::add('toastmessage', 'plugins/toastmessage/js/jquery.toastmessage.js');

        \Asset::add('base', 'js/base.js');

        $this->_warningBox = new WarningBox();

        $this->_tableGenerator = new TableGenerator();

    }

	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}